#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash

# Title for the Zenity dialog
TITLE="SSH_ASKPASS Script"

# Prompt message for the Zenity dialog
PROMPT="Enter your SSH password:"

# Use Zenity to create a password dialog
PASSWORD=$(zenity --password --title="$TITLE" --text="$PROMPT")

# Check if Zenity dialog was cancelled
if [ $? -ne 0 ]; then
    exit 1
fi

# Output the password
echo "$PASSWORD"

