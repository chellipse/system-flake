{pkgs,user,...}:

{

  environment.systemPackages = [
    pkgs.syncthing
  ];

  services.syncthing = {
    enable = true;
    package = pkgs.syncthing;
    openDefaultPorts = true;
    overrideDevices = true;
    overrideFolders = true;

    user = "${user}";
    #Defaultfolderfornewsyncedfolders
    dataDir = "/home/${user}";
    #FolderforSyncthing'ssettingsandkeys
    configDir = "/home/${user}/.syncthing/";
    settings = {
      devices = {
        "daily" = {id = "SUSF4QK-WRT3HGO-V5XI7E2-PRXPLPW-DCWT4IM-7RSMHHO-BQOJ4D7-QUIHYQQ";};
        "nixos" = {id = "WNGIV7D-5IQUUJZ-FFGFGI6-UNYW5GP-MAADOYQ-IAQE5OB-X4EDM26-FF6KMQJ";};
      };
      folders = {
        "Sync" = {
          path = "~/Sync";
          devices = [ "daily" "nixos" ];
          versioning.type = "simple";
        };
        "Music/Share" = {
          path = "~/Music/Share";
          devices = [ "daily" "nixos" ];
          versioning.type = "simple";
        };
      };
    };
  };
}
