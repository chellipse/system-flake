{

  description = "T480 NixOS Config";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11"; # Or just `nixpkgs` for the registry
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, nixos-hardware, ... }:
  let
    lib = nixpkgs.lib;
    system = "x86_64-linux";
    user = "chelll";
  in {
    nixosConfigurations = {

      daily = lib.nixosSystem {
        inherit system;

        specialArgs = {
          pkgs-unstable = import nixpkgs-unstable {
            inherit system;
          };
          user = user;
        };

        modules = [
          (./core.nix) # required setup
          (./secure.nix) # security stuff
          (./unstable.nix) # pkgs-unstable
          (./daily.nix) # daily use packages
          (./sway.nix) # sway setup
          (./lang.nix) # global lang stuff
          (./sync.nix) # syncthing
          nixos-hardware.nixosModules.lenovo-thinkpad-t480
        ];
      };

    };
  };

}

